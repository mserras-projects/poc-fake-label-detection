

import os
from src import cache_path
from src.pipelines.classify import train_model, evaluate_mlp_model
from src.pipelines.point_selection import get_best_coverage_points
from src.pipelines.prepare_training_data import prepare_all_data


def get_file(name):
    return os.path.join(cache_path, name)


def evaluate_corrupt_level(alpha: float):
    print(f'Alpha value of {alpha}\n================================')
    print("With label Semantics - Distance")
    mlp_classifier = train_model(get_file(f"data_train_alpha_{alpha}_semantic_True.pck"))
    evaluate_mlp_model(mlp_classifier,
                       get_file(f"data_test_alpha_{alpha}_semantic_True.pck"))
    print("With label semantics - No Distance")
    mlp_classifier = train_model(get_file(f"data_train_alpha_{alpha}_semantic_True_nodist.pck"))
    evaluate_mlp_model(mlp_classifier,
                       get_file(f"data_test_alpha_{alpha}_semantic_True_nodist.pck"))

    print("Without label semantics - With distance")
    mlp_classifier = train_model(get_file(f"data_train_alpha_{alpha}_semantic_False.pck"), max_iter=300)
    evaluate_mlp_model(mlp_classifier,
                       get_file(f"data_test_alpha_{alpha}_semantic_False.pck"))

    print("Without label semantics - No distance")
    mlp_classifier = train_model(get_file(f"data_train_alpha_{alpha}_semantic_False_nodist.pck"), max_iter=300)
    evaluate_mlp_model(mlp_classifier,
                       get_file(f"data_test_alpha_{alpha}_semantic_False_nodist.pck"))


if __name__ == '__main__':
    import sys
    if sys.argv[1] == "eval":
        print("Evaluating the models with the prepared data")
        for alpha in [0.1, 0.2, 0.3, 0.4, 0.5]:
            evaluate_corrupt_level(alpha)
    elif sys.argv[1] == "prepare":
        print('Obtaining the best coverage points and preparing the data to store it in cache')
        get_best_coverage_points()
        prepare_all_data()

