import math as m
import random

import numpy as np

from src.evolution import FindBestCoverage, label_fitness_function
import matplotlib.pyplot as plt


def generate_samples(num_samples):
    """
    Returns samples of a circle
    :param num_samples:
    :return:
    """
    samples = []
    for _ in range(num_samples):
        t = 2*m.pi*random.random()
        u = random.random() + random.random()
        circle_radius = (2 - u if u > 1 else u)
        samples.append([
            circle_radius*m.cos(t), circle_radius*m.sin(t)
        ])
    return np.asarray(samples)


def print_radius_example():
    samples = generate_samples(10000)
    plt.scatter(x=[s[0] for s in samples], y=[s[1] for s in samples])
    plt.show()


def test_evolution_coverage():
    num_samples = 2000
    samples_to_cover = generate_samples(num_samples)
    finder = FindBestCoverage(num_samples=num_samples, population_number=100, num_processes=10)
    best_candidate, best_score = finder.fit(
        evaluation_function=label_fitness_function, all_points=samples_to_cover, num_epochs=1000
    )
    print("Best score is: {}".format(best_score))
    plt.scatter(x=[s[0] for s in samples_to_cover], y=[s[1] for s in samples_to_cover])
    best_candidate_indexes = [i for i, item in enumerate(best_candidate.tolist()) if item > 0]
    best_x = [s[0] for s in samples_to_cover[best_candidate_indexes]]
    best_y = [s[1] for s in samples_to_cover[best_candidate_indexes]]
    plt.scatter(x=best_x, y=best_y, c="coral")
    plt.savefig("test_circle_results.png")


def test_evolution_coverage_no_limit():
    num_samples = 2000
    samples_to_cover = generate_samples(num_samples)
    finder = FindBestCoverage(num_samples=num_samples, population_number=100, num_processes=10, k=-1,
                              crossover_rate=0.2)
    best_candidate, best_score = finder.fit(
        evaluation_function=label_fitness_function, all_points=samples_to_cover, num_epochs=1000
    )
    print("Best score is: {}".format(best_score))
    plt.scatter(x=[s[0] for s in samples_to_cover], y=[s[1] for s in samples_to_cover])
    best_candidate_indexes = [i for i, item in enumerate(best_candidate.tolist()) if item > 0]
    best_x = [s[0] for s in samples_to_cover[best_candidate_indexes]]
    best_y = [s[1] for s in samples_to_cover[best_candidate_indexes]]
    plt.scatter(x=best_x, y=best_y, c="coral")
    plt.savefig("test_circle_results_nolimit.png")