import numpy as np
from scipy.spatial.distance import cosine, cdist


def calculate_euclidean_distance(point_a: np.ndarray, point_b: np.ndarray) -> float:
    """Calculates the euclidean distance between two points
    >>> p_a = np.array([1, 0, 0])
    >>> p_b = np.array([0, 1, 0])
    >>> p_c = np.array([1, 1, 1])
    >>> calculate_euclidean_distance(p_a, p_a)
    0.0
    >>> calculate_euclidean_distance(p_a, p_b)
    1.4142135623730951
    >>> calculate_euclidean_distance(p_a, p_b) == calculate_euclidean_distance(p_b, p_a)
    True
    >>> calculate_euclidean_distance(p_b, p_c)
    1.4142135623730951
    """
    return np.linalg.norm(point_a-point_b)


def calculate_cosine_distance(point_a: np.ndarray, point_b: np.ndarray) -> float:
    """
    Calculates the cosine distance between two points
    >>> p_a = np.array([1, 0, 0])
    >>> p_b = np.array([0, 1, 0])
    >>> p_c = np.array([1, 1, 1])
    >>> calculate_cosine_distance(p_a, p_a)
    0
    >>> calculate_cosine_distance(p_a, p_b)
    1
    >>> calculate_cosine_distance(p_b, p_c)
    0
    """
    return cosine(point_a, point_b)


def calculate_pointwise_distance(reference_points: np.ndarray, other_points: np.ndarray):
    """
    >>> a = np.array([[1, 1, 1], [1, 0, 1]])
    >>> calculate_pointwise_distance(a, a)
    array([[0.00000000e+00, 1.83503419e-01],
           [1.83503419e-01, 2.22044605e-16]])
    >>> b = np.array([[1, 1, 1], [1, 0, 1], [0, 0, 1], [0.6, 0.2, 0.4]])
    >>> result = calculate_pointwise_distance(a, b)
    >>> result.shape
    (2, 4)
    >>> result.T
    array([[0.00000000e+00, 1.83503419e-01],
           [1.83503419e-01, 2.22044605e-16],
           [4.22649731e-01, 2.92893219e-01],
           [7.41799002e-02, 5.50888175e-02]])
    """
    return cdist(reference_points, other_points, metric="cosine")
