import typing

from transformers import BertTokenizer, BertModel

tokenizer = BertTokenizer.from_pretrained("bert-base-uncased")

model = BertModel.from_pretrained("bert-base-uncased")


def vectorize_with_bert(input_texts: typing.List[str]):
    """
    Given a list of texts returns their pooler layer representation

    :param input_texts:
    :return:
    """
    inputs = tokenizer(input_texts, return_tensors="pt", truncation=True, padding=True)
    outputs = model(**inputs)

    return outputs.pooler_output.detach().numpy()


if __name__ == '__main__':
    output = vectorize_with_bert(["this is just a test", "I'd like some cocoa please"]*25)
    print(output)
