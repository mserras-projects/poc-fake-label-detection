import math
import os
import pickle
import typing

import numpy as np

from src import POINTS_PER_LABEL, NUM_GENETIC_EPOCHS, cache_path
from src.data import get_training_set_per_labels
from src.evolution import FindBestCoverage, label_fitness_function
from src.vectorizer import vectorize_with_bert


def select_points_per_label(all_label_samples) -> np.ndarray:
    num_samples = int(math.sqrt(len(all_label_samples)))
    finder = FindBestCoverage(num_samples=len(all_label_samples), population_number=100, num_processes=1,
                              k=num_samples)
    best_candidate, best_score = finder.fit(
        evaluation_function=label_fitness_function, all_points=all_label_samples, num_epochs=NUM_GENETIC_EPOCHS
    )
    print("Best candidate score is: {}".format(best_score))
    return best_candidate


def sort_by_length(phrases: typing.List[str]) -> typing.List[str]:
    phrases.sort(key=lambda x: len(x.split(" ")), reverse=True)
    return phrases


def filter_data(data):
    """For each label in the data filters the phrases to get the ones with max length"""
    for label in data:
        data[label] = sort_by_length(data[label])[:POINTS_PER_LABEL]
    return data


def get_best_coverage_points(low_resources=True):
    data = get_training_set_per_labels()
    if low_resources:
        data = filter_data(data)
    points_per_label = {}
    for label in data.keys():
        phrases = data[label]
        phrase_vectors = []
        for phrase in phrases:
            p_vector = vectorize_with_bert([phrase])
            phrase_vectors.append(p_vector)
        phrase_vectors = np.vstack(phrase_vectors)
        best_candidate = select_points_per_label(phrase_vectors)
        best_candidate_indexes = [i for i, item in enumerate(best_candidate.tolist()) if item > 0]
        points_per_label[label] = [
            phrase_vectors[k] for k in best_candidate_indexes
        ]
        with open(os.path.join(cache_path, 'best_representation_points.pck'), 'wb') as f:
            pickle.dump(points_per_label, f)


if __name__ == '__main__':
    get_best_coverage_points()
