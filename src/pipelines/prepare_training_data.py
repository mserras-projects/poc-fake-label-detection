import os
import pickle
import random
import typing

import numpy as np

from src.vectorizer import vectorize_with_bert
from src.data import get_training_set_per_labels, get_test_set_per_labels, LABEL_SEMANTIC_LIST
from src.spatial import cdist

from src import cache_path


def labels_to_bert() -> typing.List[str]:
    label_vectorial_representation = []
    for i in range(len(LABEL_SEMANTIC_LIST)):
        label_semantic_name = LABEL_SEMANTIC_LIST[i]
        label_vector = vectorize_with_bert([label_semantic_name])
        label_vectorial_representation.append(label_vector)
    return label_vectorial_representation


def distance_to_points(pooled_vector: np.ndarray, data_points: np.ndarray):
    """Calculates the distance from a given vector to the rest of the data-points
    pooled_vector [1, 768]
    data_points [N, 768]
    output -> [1, N]
    """
    distance_vector = cdist(pooled_vector, data_points, metric="cosine")
    assert distance_vector.shape == (1, data_points.shape[0])
    return distance_vector


def prepare_data(alpha_corrupt: float, semantic_label_representation: bool, partition: str, duplicate: bool):
    print("Loading the best points")
    # Step 1: load the best points per label
    with open(os.path.join(cache_path, 'best_representation_points.pck'), 'rb') as f:
        data_points = pickle.load(f)
    array_data_points = []
    for label in data_points:
        array_data_points += data_points[label]
    array_data_points = np.vstack(array_data_points)

    print("Loading the data: {}".format(partition))
    # Step 2: Load the data
    if partition == 'train':
        data = get_training_set_per_labels()
    elif partition == 'test':
        data = get_test_set_per_labels()
    else:
        raise Exception("Partition needs to be either train or test")

    # Step 2.1: unroll the data
    x_data, y_data, y_correct = [], [], []
    for label in data:
        x_data += data[label]
        y_data += [label]*len(data[label])
        y_correct += [1]*len(data[label])
    print("Flipping the labels with a corruption parameter of {}".format(alpha_corrupt))
    # Step 3: flip the labels
    for i in range(len(x_data)):
        label = y_data[i]
        if random.random() < alpha_corrupt:
            # Flip the label
            possible_labels = [lab for lab in range(len(LABEL_SEMANTIC_LIST)) if lab != label]
            flipped_label = random.choice(possible_labels)
            y_data[i] = flipped_label
            y_correct[i] = 0

    print("Converting the labels to a vectorial representation")
    # Step 4: convert the labels if required
    if semantic_label_representation:
        bert_labels = labels_to_bert()
        for j in range(len(y_data)):
            y_data[j] = bert_labels[y_data[j]]
    else:
        # 1-hot representation
        for j in range(len(y_data)):
            one_hot_label = np.zeros((1, len(LABEL_SEMANTIC_LIST)))
            one_hot_label[0, y_data[j]] = 1
            y_data[j] = one_hot_label

    x_distances, x_data = obtain_point_distance_matrix(array_data_points, x_data, partition=partition)

    print("Building the final matrix and saving to the cache folder")
    # Step 6: save the data in the cache folder

    train_data = np.hstack(
        [np.vstack(x_data), np.vstack(x_distances), np.vstack(y_data)]
    )
    print("Train data shape is: {}".format(train_data.shape))
    filename = "data_{}_alpha_{}_semantic_{}.pck".format(partition, alpha_corrupt, semantic_label_representation)
    with open(os.path.join(cache_path, filename), 'wb') as g:
        pickle.dump([train_data, y_correct], g)

    train_data = np.hstack(
        [np.vstack(x_data), np.vstack(y_data)]
    )
    print("Train data with NO DISTANCES shape is: {}".format(train_data.shape))
    filename = "data_{}_alpha_{}_semantic_{}_nodist.pck".format(partition, alpha_corrupt, semantic_label_representation)
    with open(os.path.join(cache_path, filename), 'wb') as g:
        pickle.dump([train_data, y_correct], g)


def obtain_point_distance_matrix(array_data_points, x_data, partition):
    # Check if this already exists on the cache data
    if os.path.exists(os.path.join(cache_path, "spatial_datapoints_{}.pck".format(partition))):
        with open(os.path.join(os.path.join(cache_path, "spatial_datapoints_{}.pck".format(partition))), 'rb') as f:
            x_distances, x_data = pickle.load(f)
            return x_distances, x_data
    print("Converting the phrases and extracting the distances")
    # Step 5: convert the phrases
    x_distances = []
    for position, phrase in enumerate(x_data):
        print("\rReplacing position {}/{}".format(position + 1, len(x_data)), end="")
        bert_pooled_representation = vectorize_with_bert(x_data[position])
        x_data[position] = bert_pooled_representation
        distances_to_anchors = distance_to_points(bert_pooled_representation, data_points=array_data_points)
        x_distances.append(distances_to_anchors)
    # Save this matrix to avoid re-computing
    with open(os.path.join(os.path.join(cache_path, "spatial_datapoints_{}.pck".format(partition))), 'wb') as f:
        pickle.dump((x_distances, x_data), f)
    return x_distances, x_data


def prepare_all_data():
    for partition in ["test", "train"]:
        for semantic_representation in [True, False]:
            for alpha in [0.1, 0.2, 0.3, 0.4, 0.5]:
                for duplicate in [True, False]:
                    prepare_data(alpha_corrupt=alpha, semantic_label_representation=semantic_representation,
                                 partition=partition, duplicate=duplicate)


if __name__ == '__main__':
    prepare_all_data()
