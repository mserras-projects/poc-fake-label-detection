import pickle
import typing

from sklearn import metrics
from sklearn.neural_network import MLPRegressor


def train_model(data_file: str, hidden_layer_size: typing.Tuple[int] = (150, 75, 30),
                activation: str = "tanh", max_iter: int = 200):
    with open(data_file, 'rb') as f:
        x, y = pickle.load(f)

    classifier = MLPRegressor(hidden_layer_sizes=hidden_layer_size,
                              activation=activation, max_iter=max_iter)
    classifier.fit(x, y)
    y_predict = classifier.predict(x)
    fpr, tpr, thresholds = metrics.roc_curve(y, y_predict, pos_label=1)
    print('AUC Train', metrics.auc(fpr, tpr))
    return classifier


def evaluate_mlp_model(model: MLPRegressor, test_file: str):
    with open(test_file, 'rb') as f:
        x, y = pickle.load(f)
    print('Input shape is: {}'.format(x.shape))
    y_predict = model.predict(x)
    fpr, tpr, thresholds = metrics.roc_curve(y, y_predict, pos_label=1)
    print('AUC', metrics.auc(fpr, tpr))
