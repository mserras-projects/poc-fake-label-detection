from numpy.random import RandomState

import os
import random

from dotenv import load_dotenv

__version__ = "1.0.0"

current_path = os.path.dirname(os.path.abspath(__file__))
cache_path = os.path.join(current_path, '..', 'cache')

if not os.path.exists(cache_path):
    os.mkdir(cache_path)

load_dotenv()
SEED = int(os.environ.get("SEED"))
POINTS_PER_LABEL = int(os.environ.get("PPL", 50))
NUM_GENETIC_EPOCHS = int(os.environ.get("NUM_GENETIC_EPOCHS", 100))
random.seed(SEED)
rs = RandomState(SEED)

