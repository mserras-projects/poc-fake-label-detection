import math
import typing
from copy import deepcopy
import numpy as np
import random
import copy
from multiprocessing import Pool

from src.spatial import calculate_pointwise_distance

"""
Given a set of data-points, a K number of points to select, and 
"""


def obtain_indexes(selected_points: np.ndarray) -> typing.List[int]:
    """
    >>> a = np.array([1, 0, 1, 0, 0, 1])
    >>> obtain_indexes(a)
    [0, 2, 5]
    >>> b = np.array([0, 0, 0, 0])
    >>> obtain_indexes(b)
    []
    """
    return np.nonzero(selected_points)[0].tolist()


def label_fitness_function(selected_points: np.ndarray, all_points: np.ndarray) -> float:
    """
    Calculates the fitness function of a selected points in terms of being representative of a whole set of points.

    We want to minimize the distance between the selected samples and all the points belonging to the cluster
    And at the same time maximize the distance between the selected samples

    The bigger the fitness function, the better.

    :return: score - float
    """
    assert selected_points.shape[0] == all_points.shape[0]
    point_indices = obtain_indexes(selected_points)

    reference_points = all_points[point_indices]

    sample_intradistantial_matrix = calculate_pointwise_distance(reference_points, reference_points)

    cluster_distance = calculate_pointwise_distance(reference_points, all_points)

    minimum_distance_per_point = sum([min(column) for column in cluster_distance.T])

    total_distance_between_selected_samples = np.sum(np.triu(sample_intradistantial_matrix))

    return total_distance_between_selected_samples - minimum_distance_per_point - np.sum(selected_points)


class BinaryEvolutionCoverage(object):
    def __init__(self, population_number,
                 crossover_rate, num_samples, k_select,
                 num_processes=1):
        """

        :param population_number:
        :param crossover_rate:
        :param num_samples:
        :param k_select:
        :param num_processes:
        """

        self.best_solutions = {}
        self.num_samples = num_samples
        self.k_select = k_select
        self.np = population_number
        self.cr = crossover_rate
        self.__current_population = []
        self.__current_scores = []
        self.__candidates = []
        self.num_processes = num_processes
        self.build_initial_population()

    def _create_candidate(self, k_select):
        """
        >>> random.seed(42)
        >>> evolution_coverage = BinaryEvolutionCoverage(1, 1, num_samples=5, sample_size=10)
        >>> evolution_coverage._create_candidate()
        array([0., 1., 1., 1., 1., 0., 0., 0., 1., 0.])
        """
        positive_indexes = random.sample(list(range(self.num_samples)), k=k_select)
        candidate = np.zeros(self.num_samples)
        candidate[positive_indexes] = 1
        return candidate

    def build_initial_population(self):
        """
        Builds the initial random population to evaluate.

        """
        if self.k_select > 0:
            k_select = self.k_select
        else:
            k_select = int(math.sqrt(self.num_samples))
        for i in range(self.np):
            candidate = self._create_candidate(k_select)
            self.__candidates.append(candidate)
            self.__current_population.append(None)
            self.__current_scores.append(-1e10)

    @staticmethod
    def array_xor(array_1, array_2):
        return (array_1 + array_2) % 2

    def mutate(self, initial_candidate, v1, v2):
        """
        Mutate the initial candidate according to other two elements
        :param initial_candidate: np.ndarray
        :param v1: np.ndarray
        :param v2: np.ndarray
        :return: np.ndarray
        """
        initial_candidate = copy.copy(initial_candidate)
        xor_sampled = self.array_xor(v1, v2)
        mutated_candidate = np.clip(initial_candidate+xor_sampled, a_max=1, a_min=0)
        return mutated_candidate

    def binomial_crossover(self, x, v):
        u = deepcopy(x)
        for i in range(x.shape[0]):
            random_num = random.random()
            if random_num <= self.cr:
                u[i] = v[i]
        u = self.__safe_mutation(u)
        return u

    def crossover_population(self):
        for j, member in enumerate(self.__current_population):
            sample_population = [i for i in range(len(self.__current_population)) if i != j]
            m1, m2 = random.sample(sample_population, k=2)
            v_mutated = self.mutate(member,
                                    self.__current_population[m1],
                                    self.__current_population[m2])
            member_crossover = self.binomial_crossover(member, v_mutated)
            self.__candidates[j] = member_crossover

    def get_candidate(self, candidate_index):
        return self.__candidates[candidate_index]

    def set_score(self, candidate_index, score):
        current_score = self.__current_scores[candidate_index]
        if current_score < score:
            current_candidate = deepcopy(self.__candidates[candidate_index])
            self.__current_population[candidate_index] = current_candidate
            self.__current_scores[candidate_index] = score
            print('Candidate {} improved score from {} to {}'.format(
                candidate_index,
                current_score,
                score
            ))

    def get_best_candidate(self):
        max_score = max(self.__current_scores)
        index = self.__current_scores.index(max_score)
        return self.__current_population[index], max_score

    @property
    def population(self):
        return self.__current_population

    @property
    def scores(self):
        return self.__current_scores

    def set_candidates(self, candidates):
        self.__candidates = candidates

    def __safe_mutation(self, mutated_candidate: np.ndarray) -> np.ndarray:
        """
        Assures that the mutation has strictly the fixed positive items to sample (k_select)

        """
        positive_sample_indexes = np.argwhere(mutated_candidate > 0)
        num_positive_items = positive_sample_indexes.shape[0]
        if self.k_select < 0:
            # Just ensure that at least one item is equal to 1
            if num_positive_items > 0:
                return mutated_candidate
            else:
                selected_for_adding = random.choice(range(len(mutated_candidate)))
                mutated_candidate[selected_for_adding] = 1
        else:
            if num_positive_items == self.k_select:
                return mutated_candidate
            elif num_positive_items > self.k_select:
                # We have to remove some of them
                samples_to_remove = [subarray[0] for subarray in positive_sample_indexes]
                selected_for_removal = random.sample(samples_to_remove, k=abs(num_positive_items-self.k_select))
                mutated_candidate[selected_for_removal] = 0
            else:
                null_sample_indexes = np.argwhere(mutated_candidate == 0)
                samples_to_add = [subarray[0] for subarray in null_sample_indexes]
                selected_for_adding = random.sample(samples_to_add, k=abs(num_positive_items - self.k_select))
                mutated_candidate[selected_for_adding] = 1
        return mutated_candidate


class FindBestCoverage(object):
    def __init__(self, num_samples, population_number, k=10, crossover_rate=0.6,
                 num_processes=1):
        self.population_number = population_number
        self.shape = num_samples
        self.crossover_rate = crossover_rate

        self.model = BinaryEvolutionCoverage(population_number=population_number,
                                             crossover_rate=crossover_rate, num_samples=num_samples,
                                             k_select=k, num_processes=num_processes)
        self.num_processes = num_processes

    def eval_candidate(self, *args):
        index, evaluation_function, all_points = args[0]
        candidate = self.model.get_candidate(index)
        score = evaluation_function(candidate, all_points)
        return index, score

    def fit(self, evaluation_function, all_points, num_epochs=100) -> typing.Tuple[np.ndarray, float]:
        for epoch in range(num_epochs):
            print("Starting epoch {}/{}".format(epoch+1, num_epochs))
            # start_time = datetime.datetime.now()
            with Pool(self.num_processes) as p:
                arguments = [(i, evaluation_function, all_points) for i in range(self.population_number)]
                results = p.map(self.eval_candidate, arguments)
                for index, score in results:
                    self.model.set_score(index, score)
            self.model.crossover_population()
        return self.model.get_best_candidate()


