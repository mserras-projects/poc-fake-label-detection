import typing

from sklearn.datasets import fetch_20newsgroups

from src.clean import document_to_phrases

LABEL_TRANSFORM = {
    'alt.atheism': "atheism",
    'comp.graphics': "computer graphics",
    'comp.os.ms-windows.misc': "computer operating systems windows miscellaneous",
    'comp.sys.ibm.pc.hardware': "computer system ibm personal computer hardware",
    'comp.sys.mac.hardware': "computer system macintosh hardware",
    'comp.windows.x': "computer windows X",
    'misc.forsale': "for sale",
    'rec.autos': "autos",
    'rec.motorcycles': "motorcycles",
    'rec.sport.baseball': "baseball",
    'rec.sport.hockey': "hockey",
    'sci.crypt': "cryptography",
    'sci.electronics': "electronics",
    'sci.med': "medicine",
    'sci.space': "space",
    'soc.religion.christian': "christian religion",
    'talk.politics.guns': "politics and guns",
    'talk.politics.mideast': "middle east politics",
    'talk.politics.misc': "politics",
    'talk.religion.misc': "religion"
}

LABEL_SEMANTIC_LIST = [
    "atheism", "computer graphics", "computer operating systems windows miscellaneous",
    "computer system ibm personal computer hardware", "computer system macintosh hardware",
    "computer windows X", "for sale", "automobile", "motorcycles", "baseball", "hockey", "cryptography",
    "electronics", "medicine", "space", "christian religion", "politics and guns", "middle east politics",
    "politics", "religion"
]


def transform_label(label_integer: int, original_label_list: typing.List[str]):
    label_text = original_label_list[label_integer]
    return LABEL_TRANSFORM[label_text]


def get_phrases_with_labels(data):
    data_phrases, data_labels, transformed_labels = [], [], []
    for text, label in zip(data.get("data"), data.get("target")):
        phrases = document_to_phrases(text, double_jump=True)
        semantic_label = transform_label(label, original_label_list=data.get("target_names"))
        data_phrases += phrases
        data_labels += [label]*len(phrases)
        transformed_labels += [semantic_label]*len(phrases)
    return data_phrases, data_labels, transformed_labels


def get_reuters():
    train_data = fetch_20newsgroups(subset="train")

    test_data = fetch_20newsgroups(subset="test")

    train_phrases, train_labels, train_transformed_labels = get_phrases_with_labels(train_data)

    test_phrases, test_labels, test_transformed_labels = get_phrases_with_labels(test_data)

    return {
        "train": {"phrases": train_phrases, "labels": train_labels, "semantic_labels": train_transformed_labels},
        "test": {"phrases": test_phrases, "labels": test_labels, "semantic_labels": test_transformed_labels}
    }


def get_training_set_per_labels():
    data = get_reuters()
    training_phrases = data.get("train").get("phrases")
    training_labels = data.get("train").get("labels")

    training_data_split_by_labels = {
        label: [phrase for i, phrase in enumerate(training_phrases) if training_labels[i] == label] for label
        in set(training_labels)
    }
    return training_data_split_by_labels


def get_test_set_per_labels():
    data = get_reuters()
    test_phrases = data.get("test").get("phrases")
    test_labels = data.get("test").get("labels")

    test_data_split_by_labels = {
        label: [phrase for i, phrase in enumerate(test_phrases) if test_labels[i] == label] for label
        in set(test_labels)
    }
    return test_data_split_by_labels
