"""Simple cleaning and formatting of the documents before preprocessing"""
import typing
import re

MIN_WORDS = 10
UNWANTED_CHARS = ["?", "!", ":", '"', "|", ">", "<", "\t", "(", ")", "+",
                  "-", "*", "[", "]", "^", "#", "}", "{", "=", "_", "/", ";", "%", "\\", "~", '\n', '.', ",", "$"]


def _remove_unwanted_chars(text: str) -> str:
    """
    We want to change for whitespace these unwanted chars as they introduce too much hetereogenity in the
    vocabulary
    """
    for char in UNWANTED_CHARS:
        text = text.replace(char, " ")
    text = text.strip(" ")
    text = " ".join([word for word in text.split(" ") if word])
    return text


def _replace_numbers(text: str) -> str:
    """
    >>> _replace_numbers("Tengo 10.2 cosas y $20.5 en el banco con 12 años y 0.56 neuronas")
    'Tengo NUM cosas y $NUM en el banco con NUM años y NUM neuronas'
    """
    regex_pattern = r"[-+]?[.]?[\d]+(?:,\d\d\d)*[\.]?\d*(?:[eE][-+]?\d+)?"
    results = re.findall(regex_pattern, text)
    for result in results:
        text = text.replace(result, "[NUM]")
    return text


def _replace_emails(text: str) -> str:
    regex_pattern = r'[\w.+-]+@[\w-]+\.[\w.-]+'
    results = re.findall(regex_pattern, text)
    for result in results:
        text = text.replace(result, "email@email.com")
    return text


def document_to_phrases(document: str, double_jump: bool) -> typing.List[str]:
    separator = "\n\n" if double_jump else "\n"
    document = extract_patterns(document)
    phrases = [_remove_unwanted_chars(phrase) for phrase in document.split(separator)]  # We want to use the double jump
    return [phrase.lower() for phrase in phrases if len(phrase.split(" ")) > MIN_WORDS]   # Remove empty and low item ones


def extract_patterns(document):
    document = _replace_emails(document)
    document = _replace_numbers(document)
    return document.lower()