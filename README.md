# Proof of Concept - Using BERT, Genetic Label Coverage & Labels' Semantics for Fake Label Detection

Proof of concept of a methodology to exploit the semantic knowledge of the labels
to detect / weight if a given sample belongs to a class or not. Additionally, a cluster-coverage genetic algorithm
has been implemented to select the most representative samples to describe the manifold of each label in the BERT representation.
Reuters News dataset is used.

In a nutshell, it employs the BERT model to cast each sample to a 768 dimensional point.
Then, for each label, a cluster-coverage evolutionary algorithm is employed to sample those points
that better cover this label-space while also being as distant as possible from each other.

Finally, and to classify a new sample, the following information is used:

  - The BERT pooler layer representation of the sample x_{pooler}^i
  - The cosine distance of the x_{pooler}^i w.r.t each point of the label-clusters representative samples
  - The label representation either in 1-hot or extracted from the BERT pooled layer

Then, a Multi-Layer Perceptron is trained given an artificially corrupted training set in a Binary Regression
task.

The weight resulting of this classifier can be used to address issues with noisy-labels in
text classification:

  - Sample Filtering: remove those samples which can be corrupted (use this method as a Binary Classifier)
  - Loss re-weighting: re-weight the loss function using the output of this regressor
  - Active Learning: use it as a metric to extract and fix those dubious samples

# Results & Conclusions

In the following table we can see the results obtained over the different Test sets.
As we are working on a binary regression, the AUC is used:

## Reuters Corpus
| % Corrupt | Point Distance | Semantic Label | Test AUC Score |
|-----------|----------------|----------------|----------------|
| 0.1       | Yes            | Yes            | 0.6541         |
| 0.1       | Yes            | No             | 0.7434         |
| 0.1       | Yes            | No             | 0.7364         |
| 0.1       | No             | No             | **0.8381**     |
| //////    | ////           | ////           | /////          |
| 0.2       | Yes            | Yes            | 0.83779        |
| 0.2       | Yes            | No             | **0.86036**    |
| 0.2       | No             | Yes            | 0.8459         |
| 0.2       | No             | No             | 0.8554         |
| //////    | ////           | ////           | /////          |
| 0.3       | Yes            | Yes            | 0.87399        |
| 0.3       | Yes            | No             | **0.88619**    |
| 0.3       | No             | Yes            | 0.85564        |
| 0.3       | No             | No             | 0.88197        |
| //////    | ////           | ////           | /////          |
| 0.4       | Yes            | Yes            | 0.87459        |
| 0.4       | Yes            | No             | **0.88771**    |
| 0.4       | No             | Yes            | 0.8654         |
| 0.4       | No             | No             | 0.8680         |
| //////    | ////           | ////           | /////          |
| 0.5       | Yes            | Yes            | 0.86959        |
| 0.5       | Yes            | No             | **0.8867**     |
| 0.5       | No             | Yes            | 0.8657         |
| 0.5       | No             | No             | 0.8712         |
| //////    | ////           | ////           | /////          |

Overall, the best performing method is the one which uses the distances w.r.t the label-cluster points extracted
with the genetic algorithm. 

The semantics of the labels do not improve the results, they even make it harder for the classifier to sample the label
as correct or incorrect.

Nevertheless, this might be because the algorithm of choice (MLP) cannot relate those inputs.

# Usage

Install the required packages with

    pip3 install -r requirements.txt

You can prepare the augmented datasets with:

    python3 main.py prepare

After the data is prepared, you can train and evaluate the models (to obtain the reported numbers) with

    python3 main.py eval

## Current Limitations & Future Work
There are some limitations with the experiments carried out in this repo:

### Model

A Multi-Layer Perceptron is used as it has the capability of encoding the information
of the whole representations, yet, this is still considered a lightweight NN, which could
be further improved.

### FW: Plot the T-NS representations 

Plot the results of the genetic coverage algorithm in a more realistic scenario
to check if it is valid.


### It may help to corrupt every sample of the training set?

Only for the training, so the network knows where is the breach for every situation.
Maybe it is overkill. Duplicating the corrupted points (i.e. keeping the original one in the training set) might help

## Next Steps

1- Create a library to perform these experiments

2- Combine this with BERT fine-tuning

3- Perform more experiments over different corpus

4- Allow more data parametrisation (as duplications of the flipped samples and so on)

5- Compare it with other simpler approaches